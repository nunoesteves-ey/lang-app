# Lang App

React project for <Academia de Código\_>'s javascript training.

# Development

Clone the repository, then from the commandline:

```shell
$ yarn install
$ yarn test
$ yarn lint
```

To run the live server locally, you can then use `yarn start`.

To preview a production build:

```shell
$ yarn build
$ yarn serve -s build
```

__Note:__ you might need to empty the local cache or force a reload (^R) to
view the fresh version instead of old builds.
