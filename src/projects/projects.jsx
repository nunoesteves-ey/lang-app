import React from 'react';
import { Grid, Segment } from 'semantic-ui-react';

import NavMenu from '../components/nav-menu';
import ProjectCardDisplay from './project-card-display';
import Pagination from '../components/pagination';
import { searchByLanguage } from '../services/github-api';
import DimmerLoader from '../components/dimmer-loader';

const elements = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

class Projects extends React.Component {
    state = {
        items: [
            { name: 'all', label: 'All' },
            { name: 'java', label: 'Java' },
            { name: 'javascript', label: 'Javascript' },
            { name: 'ruby', label: 'Ruby' },
            { name: 'python', label: 'Python' },
            { name: 'c', label: 'C' },
            { name: 'c++', label: 'C++' }
        ],
        page: 1,
        loading: false,
        activeItem: 'all',
        projects: elements
    }

    componentDidMount = () => {
        this.fetchProjects(this.state.activeItem);
    }

    onSelectHandler = (item) => {
        this.setState({ activeItem: item, page: 1 });

        this.fetchProjects(item, 1);
    }

    onPageChangeHandler = (e, { activePage }) => {
        this.fetchProjects(this.language, activePage);
    }

    fetchProjects = async (language, page) => {
        this.setState({ projects: elements, loading: true });

        try {
            const { items } = await searchByLanguage(language, page);
            this.setState({ projects: items });
        } catch(error) {
            this.setState({ error: `Error fetching projects: ${error.message}.`});
        } finally {
            this.setState({ loading: false });
        }
    }

    render() {
        const { items, activeItem, projects, pagination, loading } = this.state;
        const onSelect = this.onSelectHandler;

        return (
            <Grid padded centered>
                <Grid.Row>
                    <NavMenu className="compact"
                        items={items}
                        activeItem={activeItem}
                        onClickHandler={onSelect} />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column floated='right'>
                        <Segment basic floated='right'>  
                            <Pagination />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <DimmerLoader loading={loading}>
                        <ProjectCardDisplay projects={projects} onPageChangeHandler={this.onPageChangeHandler} />
                    </DimmerLoader>
                </Grid.Row>
            </Grid>
        );
    }
}

export default Projects;
