import React from 'react';
import PropTypes from 'prop-types';
import { Card, Icon } from 'semantic-ui-react';

import Pagination from '../components/pagination';
import ProjectCard from '../components/github-card';

let index = 0;

const ProjectCardDisplay = ({ projects }) => (
    <Card.Group centered>
        { projects.map(project => (
            <ProjectCard key={index++} imageUrl={project.avatar} title={project.name} subtitle={project.owner} url={project.url} >
                {project.stars ? (<a href={project.starsUrl} target='_blank'>
                    <Icon name='star' />
                    {project.stars} stars
                </a>) : null}
            </ProjectCard>
        ))}
    </Card.Group>
);

ProjectCardDisplay.propTypes = {
    projects: PropTypes.array
};

export default ProjectCardDisplay;
