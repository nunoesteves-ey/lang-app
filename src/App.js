import React from 'react';
import { Link, Switch, Route } from 'react-router-dom';

import { Grid } from 'semantic-ui-react';
import './App.css';

import NavMenu from './components/nav-menu';
import Home from './home';
import Projects from './projects/projects';
import Profiles from './profiles/profiles';

const navItems = [
    { name: '#', label: 'Home', as: Link, to: '/' },
    { name: 'projects', label: 'Projects', as: Link, to: '/projects' },
    { name: 'profiles', label: 'Profiles', as: Link, to: '/profiles' }
];

const MainNavigation = () => (
    <Route render={({ location }) => {
        const activeItem = location.pathname.split('/')[1] || '#';

        return <NavMenu className='huge inverted borderless' activeItem={activeItem} items={navItems} />;
    }} />
);

const App = () => (
    <div>
        <MainNavigation />
        <Grid padded centered>
            <Grid.Row>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/projects" component={Projects} />
                    <Route path="/profiles" component={Profiles} />
                    <Profiles />
                </Switch>
            </Grid.Row>
        </Grid>
    </div>
);

export default App;
