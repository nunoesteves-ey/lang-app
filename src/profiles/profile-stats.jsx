import React from 'react';
import PropTypes from 'prop-types';
import { Statistic } from 'semantic-ui-react';

const ProfileStatistics = ({ statistics }) => (
    <Statistic.Group widths='3' size='mini'>
        { Object.keys(statistics).map(key => (
            <Statistic key={key}>
                <Statistic.Value>{statistics[key]}</Statistic.Value>
                <Statistic.Label>{key}</Statistic.Label>
            </Statistic>
        ))}
    </Statistic.Group>
);

ProfileStatistics.propTypes = {
    statistics: PropTypes.object
};

ProfileStatistics.defaultProps = {
    statistics: {}
};

export default ProfileStatistics;
