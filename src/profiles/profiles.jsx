import React from 'react';
import { Grid } from 'semantic-ui-react';

import { getProfileData } from '../services/github-api';
import UsernameForm from './username-form';
import ProfileCard from '../components/github-card';
import ProfileStatistics from './profile-stats';
import DimmerLoader from '../components/dimmer-loader';

class Profiles extends React.Component {
    state = {
        profile: {},
        loading: false
    }

    onSubmitHandler = (username) => {
        this.fetchProfileData(username);
    }

    fetchProfileData = async (username) => {
        this.setState({ profile: {}, loading: true });

        const { profile } = await getProfileData(username);

        this.setState({ profile: profile, loading: false });
    }

    render() {
        const onSubmitHandler = this.onSubmitHandler;
        const { profile, loading } = this.state;
        const { username, avatar, url, repos, followers, stars } = profile;
        const stats = { repos, followers, stars };

        return (
            <Grid padded centered>
                <Grid.Row>
                    <UsernameForm onSubmit={onSubmitHandler} />
                </Grid.Row>
                <Grid.Row>
                    <DimmerLoader loading={loading}>
                        <ProfileCard imageUrl={avatar} title={username} url={url}>
                            { username && <ProfileStatistics statistics={stats} />}
                        </ProfileCard>
                    </DimmerLoader>
                </Grid.Row>
            </Grid>
        );
    }
}

export default Profiles;