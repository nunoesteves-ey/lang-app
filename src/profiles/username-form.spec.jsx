import React from 'react';
import { mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import UsernameForm from './username-form';

describe('UsernameForm', () => {
    const minProps = {
        onSubmit: () => {}
    };

    it('renders without exploding', () => {
        expect(mount(<UsernameForm {...minProps} />)).toHaveLength(1);
    });

    it('updates the username on changes to the input', () => {
        const wrapper = mount(<UsernameForm {...minProps} />);

        wrapper.find('input').simulate('change', {
            target: { value: 'an username' }
        });

        expect(wrapper.state().username).toBe('an username');
    });

    it('calls the handler when submitted', () => {
        const onSubmitMock = jest.fn();

        const wrapper = mount(<UsernameForm {...minProps} onSubmit={onSubmitMock} />);

        wrapper.find('form').simulate('submit');

        expect(onSubmitMock).toHaveBeenCalled();
    });
});