import React from 'react';
import PropTypes from 'prop-types';

import { Button, Form } from 'semantic-ui-react';

class UsernameForm extends React.Component {
    state = {
        username: ''
    }

    handleChange = e => this.setState({ username: e.target.value });

    render() {
        const { onSubmit } = this.props;
        const { username } = this.state;

        return (
            <Form onSubmit={onSubmit.bind(null, username)}>
                <Form.Field>
                    <label>Github Username:</label>
                    <input value={this.state.username}
                        onChange={this.handleChange}
                        placeholder='Github Username' />
                </Form.Field>
                <Button type='submit'>Search</Button>
            </Form>
        );
    }
}

UsernameForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
};

export default UsernameForm;