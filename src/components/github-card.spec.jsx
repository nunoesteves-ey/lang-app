import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import GithubCard from './github-card';

describe('GithubCard', () => {
    const minProps = {};

    it('renders without exploding', () => {
        expect(shallow(<GithubCard {...minProps} />)).toHaveLength(1);
    });

    it('renders a Card component', () => {
        const wrapper = shallow(<GithubCard {...minProps} index={1} />);
        
        expect(wrapper.find('Card')).toHaveLength(1);
        expect(wrapper.find('Label')).toHaveLength(1);
    });

    it('renders a github card', () => {
        expect(toJson(shallow(<GithubCard />))).toMatchSnapshot(); 
    });
});