import React from 'react';
import PropTypes from 'prop-types';
import { Menu } from 'semantic-ui-react';

const MenuItem = ({ name, label, active, onClickHandler, ...itemProps }) => (
    <Menu.Item name={name} active={active} onClick={onClickHandler} {...itemProps}>
        {label}
    </Menu.Item>
);

MenuItem.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    active: PropTypes.bool,
    onClickHandler: PropTypes.func
};

MenuItem.defaultProps = {
    active: false,
    onClickHandler: () => {}
};

class NavMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeItem: props.activeItem
        };
    }

    baseClickHandler = itemKey => {
        this.setState({ activeItem: itemKey });

        this.state.onClickHandler(itemKey);
    };

    render() {
        const { items, className } = this.props;
        const { activeItem } = this.state;

        return (
            <Menu className={className}>
                {items.map(({ name, label, ...itemProps }) => (
                    <MenuItem
                        key={name}
                        name={name}
                        label={label}
                        active={name === activeItem}
                        onClickHandler={e => this.baseClickHandler(name)}
                        {...itemProps}>
                        {label}
                    </MenuItem>
                ))}
            </Menu>
        );
    }
}

NavMenu.propTypes = {
    className: PropTypes.string,
    items: PropTypes.array,
    activeItem: PropTypes.string.isRequired,
    inverted: PropTypes.bool,
    borderless: PropTypes.bool,
    style: PropTypes.string
};

NavMenu.defaultProps = {
    items: [],
    className: null,
    inverted: false,
    borderless: false,
    style: null
};

export default NavMenu;
