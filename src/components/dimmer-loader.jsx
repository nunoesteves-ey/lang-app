import React from 'react';
import PropTypes from 'prop-types';
import { Dimmer, Loader, Segment } from 'semantic-ui-react';

const DimmerLoader = ({ children, loading }) => (
    <Dimmer.Dimmable as={Segment} dimmed={loading}>
        <Dimmer active={loading} inverted>
            <Loader>Loading</Loader>
        </Dimmer>
        {children}
    </Dimmer.Dimmable>
);

DimmerLoader.propTypes = {
    children: PropTypes.node,
    loading: PropTypes.bool
};

DimmerLoader.defaultProps = {
    loading: true
};

export default DimmerLoader;