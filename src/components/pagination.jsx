import React from 'react';
import PropTypes from 'prop-types';
import { Pagination } from 'semantic-ui-react';

const PaginationComponent = ({ page, totalPages, nextPage, prevPage, onPageChange }) => (
    <Pagination activePage={page}
        totalPages={totalPages}
        onPageChange={onPageChange}
        siblingRange='1' />
);

PaginationComponent.propTypes = {
    page: PropTypes.number,
    totalPages: PropTypes.number,
    nextPage: PropTypes.number,
    prevPage: PropTypes.number,
    onPageChange: PropTypes.func.isRequired
};

PaginationComponent.defaultProps = {
    page: 1,
    totalPages: 1,
    nextPage: null,
    prevPage: null
};

export default PaginationComponent;