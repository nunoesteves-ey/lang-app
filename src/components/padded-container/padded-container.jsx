import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';

import './padded-container.css';

const PaddedContainer = ({ children }) => (
    <Container className='padded-container'>
        {children}
    </Container>
);

PaddedContainer.propTypes = {
    children: PropTypes.node
};

export default PaddedContainer;