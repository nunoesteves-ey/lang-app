import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label } from 'semantic-ui-react';

import PaddedContainer from './padded-container/padded-container';

const placeholderImage = require('../assets/placeholder-image.png');
const placeholderText  = require('../assets/placeholder-text.png');

const GithubCard = ({ index, imageUrl, title, subtitle, url, children }) => (
    <Card header={title} href={url}>
        <PaddedContainer>
            { index && <Label attached='top left'>#{index}</Label> }
            <Image src={imageUrl || placeholderImage}
                centered circular bordered width='262px' height='262px' />
            <label>{title}</label>
        </PaddedContainer>
        <Card.Meta>{subtitle}</Card.Meta>
        <Card.Content>
            {children || <Image src={placeholderText} />}
        </Card.Content>
    </Card>
);

GithubCard.propTypes = {
    index: PropTypes.number,
    imageUrl: PropTypes.string,
    title: PropTypes.string,
    subtitle: PropTypes.string,
    url: PropTypes.string,
    children: PropTypes.node
};

export default GithubCard;


