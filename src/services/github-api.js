import fakeData from './fake-github-data';
import axios from 'axios';

const baseUrl = 'https://api.github.com';

const apiRequest = async function(endpoint, opts={}) {

    const response = await axios.get(`${baseUrl}/${endpoint}`, {
        params: opts.params,
        headers: {
            'accept': 'application/vnd.github.mercy-preview+json' 
        },
        responseType: 'json'
    });

    return response.data;
};

const parseItems = function(items) {
    return items.map((item) => {
        return {
            id: item.id,
            name: item.name,
            url: item.html_url,
            owner: item.owner.login,
            avatar: item.owner.avatar_url,
            stars: item.stargazers_count,
            starsUrl: item.stargazers_url
        };
    });
};

const parseProfile = function(profile) {
    return {
        username: profile.login,
        avatar: profile.avatar_url,
        url: profile.html_url,
        repos: profile.public_repos,
        followers: profile.followers,
        following: profile.following
    };
};

const getProfileStars = async (user) => {
    const repos = await apiRequest(`users/${user}/repos`);

    const stars = repos.reduce((acc, el) => acc + el.stargazers_count, 0);

    return { stars };
};

const getProfile = async (user) => {
    const profile = await apiRequest(`users/${user}`);

    const parsedProfile = parseProfile(profile);

    return parsedProfile;
};

const getFakeData = async (language) => {
    const { items } = fakeData;
    return items.map((item) => {
        return {
            id: item.id,
            name: item.name,
            html_ulr: item.html_url,
            owner: item.owner.login,
            avatar: item.owner.avatar_url
        };
    });
};

const searchByLanguage = async function(language, page=1) {
    const { items } = await apiRequest(`search/repositories?q=stars:>1+language:${encodeURIComponent(language)}`, {
        params: {
            sort: 'stars',
            order: 'desc',
            type: 'Repositories',
            per_page: '12',
            page: page
        }
    });

    const parsedItems = parseItems(items);
        
    return { items: parsedItems };
};

const getProfileData = async (user) => {
    const profile = await Promise.all([getProfile(user), getProfileStars(user)])
        .then(([baseProfile, stars]) => ({ ...baseProfile, ...stars }));

    return { profile: profile };
};

export { searchByLanguage, getProfileData, getFakeData };
